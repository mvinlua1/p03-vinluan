# Doodlejump (Project 3)
Marvin Vinluan (mvinlua1@binghamton.edu)
11 June 2017

Object: Break all the blocks, using the slider to affect sideways motion of the red bouncing square.  Blue blocks turn green on first hit, and disappear with the second.

If the red square reaches the bottom, another blue block will appear in a random location as a penalty.

If you break all the blocks, a new set of blocks will spawn, and you'll have to start over again, because that's how life works sometimes.
